# issue-220944-with-freeze-period

Freeze period created from MON - SAT

```bash
curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: <redacted>" \
     --data '{ "freeze_start": "0 0 * * MON", "freeze_end": "0 0 * * SAT", "cron_timezone": "UTC" }' \
     --request POST https://gitlab.com/api/v4/projects/19454491/freeze_periods
{"id":39,"freeze_start":"0 0 * * MON","freeze_end":"0 0 * * SAT","cron_timezone":"UTC","created_at":"2020-06-18T11:01:16.715Z","updated_at":"2020-06-18T11:01:16.715Z"}


curl --header "PRIVATE-TOKEN: <redacted>" "https://gitlab.com/api/v4/projects/19454491/freeze_periods"
[{"id":39,"freeze_start":"0 0 * * MON","freeze_end":"0 0 * * SAT","cron_timezone":"UTC","created_at":"2020-06-18T11:01:16.715Z","updated_at":"2020-06-18T11:01:16.715Z"}]
```